const conexion = require('./../config/bdconfig');

module.exports = class Estudios {
    constructor(estJSON) {}

    static getVentasFecha(fecha1, fecha2) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM cuenta c WHERE c.FECHA_APERTURA BETWEEN DATE_FORMAT(?, '%Y-%m-%d') AND DATE_FORMAT(?, '%Y-%m-%d') ",
            [fecha1, fecha2],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getVentasHechosBfos(fecha1, fecha2) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM dw_easy_local.hc_d_diario_catalogo WHERE fecha_entrada BETWEEN DATE_FORMAT(?, '%Y-%m-%d') AND DATE_FORMAT(?, '%Y-%m-%d')",
            [fecha1, fecha2],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getProductosHechosAux(fecha1, fecha2) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM dw_easy_local.hc_d_diario_aux WHERE fecha_carga BETWEEN DATE_FORMAT(?, '%Y-%m-%d') AND DATE_FORMAT(?, '%Y-%m-%d')",
            [fecha1, fecha2],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }
}