const conexion = require('./../config/bdconfig');

module.exports = class Productos {
    constructor() {}

    static TransactionCreator() {
        return new Promise((resolve, reject) =>{
            conexion.beginTransaction
            conexion.query("START TRANSACTION;"+
            "INSERT INTO usuarios VALUES (null, 0, 'DADDY YONKY', '1255', NULL, 0, 'ROLE_USER');"+
            "DELETE FROM U using usuarios as U WHERE U.id_usuario = 4;"+
            "UPDATE usuarios U SET U.nombre = 'PACO' WHERE U.id_usuario = 20;"+
            "DELETE FROM U using usuarios as U WHERE U.id_usuario = 4;"+
            "select ((timestampdiff(SECOND, c.fecha_apertura, c.fecha_cierre)/60)/60)-1 as 'diferecia' from cuenta c where C.id_cuenta = 21;"+
            "COMMIT;",
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }
}