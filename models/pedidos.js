const conexion = require('./../config/bdconfig');

module.exports = class Pedido {
    constructor(pedidoJSON) {
        this.ID_PEDIDO = pedidoJSON.id_pedido || -1;
        this.ID_CUENTA = pedidoJSON.id_cuenta || null;
        this.ID_PRODUCTO = pedidoJSON.id_producto || null;
        this.CANTIDAD = pedidoJSON.cantidad || null;
        this.PRECIO_FINAL = pedidoJSON.precio_final || 0;
        this.COSTE_FINAL = pedidoJSON.coste_final || 0;
        this.VAT_FINAL = pedidoJSON.vat_final || 0;
        this.FECHA = pedidoJSON.fecha || null;
    }

    nuevoPedido() {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_CUENTA: this.ID_CUENTA,
                ID_PRODUCTO: this.ID_PRODUCTO,
                CANTIDAD: this.CANTIDAD,
                PRECIO_FINAL: this.PRECIO_FINAL,
                COSTE_FINAL: this.COSTE_FINAL,
                VAT_FINAL: this.VAT_FINAL
            };
            conexion.query("INSERT INTO PEDIDOS SET ? ",
            datos, (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado.insertId);
            })
        })
    }

    static getPedido(id_cuenta, id_order) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM PEDIDOS P NATURAL JOIN PRODUCTOS WHERE P.ID_PEDIDO = ? AND P.ID_CUENTA = ? ",
            [id_order, id_cuenta],
            (error, resultado, campos) =>{

                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getAllPedidos(id_account) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM PEDIDOS P NATURAL JOIN PRODUCTOS WHERE P.ID_CUENTA = ? ",
            id_account,
            (error, resultado, campos) =>{

                if (error) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    updatePedido(id_order, id_account) {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_CUENTA: this.ID_CUENTA,
                ID_PRODUCTO: this.ID_PRODUCTO,
                CANTIDAD: this.CANTIDAD,
                PRECIO_FINAL: this.PRECIO_FINAL,
                COSTE_FINAL: this.COSTE_FINAL,
                VAT_FINAL: this.VAT_FINAL
            };
            conexion.query("UPDATE PEDIDOS SET ? WHERE ID_PEDIDO = ? AND ID_CUENTA = ? ",
            [datos, id_order, id_account],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static deletePedido(id_order, id_cuenta) {
        return new Promise((resolve, reject) =>{
            conexion.query("DELETE FROM PEDIDOS WHERE ID_PEDIDO = ? AND ID_CUENTA = ?",
            [id_order, id_cuenta],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }
}