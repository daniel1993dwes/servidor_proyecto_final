const conexion = require('./../config/bdconfig');

module.exports = class Cuentas {
    constructor(cuentasJSON) {
        this.ID_CUENTA = cuentasJSON.id_cuenta || -1;
        this.ID_MESA = cuentasJSON.id_mesa || null;
        this.ID_USUARIO = cuentasJSON.id_usuario || null;
        this.ID_RESTAURANTE = cuentasJSON.id_restaurante || null;
        this.TOTAL_B = cuentasJSON.total_b || 0;
        this.TOTAL_C = cuentasJSON.total_c || 0;
        this.TOTAL_VAT = cuentasJSON.total_vat || 0;
        this.DESCRIPCION = cuentasJSON.descripcion || null;
        this.FECHA_APERTURA = cuentasJSON.fecha_apertura || null;
        this.FECHA_CIERRE = cuentasJSON.fecha_cierre || null;
        this.ESTADO = cuentasJSON.estado || 'OPEN';
    }

    nuevaCuenta() {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_MESA: this.ID_MESA,
                ID_USUARIO: this.ID_USUARIO,
                ID_RESTAURANTE: this.ID_RESTAURANTE,
                TOTAL_B: this.TOTAL_B,
                TOTAL_C: this.TOTAL_C,
                TOTAL_VAT: this.TOTAL_VAT,
                DESCRIPCION: this.DESCRIPCION,
                FECHA_APERTURA: this.FECHA_APERTURA,
                FECHA_CIERRE: this.FECHA_CIERRE,
                ESTADO: this.ESTADO
            };
            conexion.query("INSERT INTO CUENTA SET ? ",
            datos, (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getCuentaClosedId(id_acc, id_rest, id_user) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM CUENTA WHERE ID_CUENTA = ? AND ID_RESTAURANTE = ? AND ID_USUARIO = ? AND ESTADO = 'CLOSED' AND FECHA_CIERRE = (SELECT MAX(FECHA_CIERRE) FROM CUENTA)",
            [id_acc, id_rest, id_user],
            (error, resultado, campos) =>{
                if (error) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getAllCuentasCerradasMesa(id_table, id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM CUENTA c WHERE ID_MESA = ? AND ID_RESTAURANTE = ? AND (c.estado = 'CLOSED') AND c.FECHA_CIERRE = (SELECT MAX(FECHA_CIERRE) FROM CUENTA)",
            [id_table, id_rest],
            (error, resultado, campos) =>{
                if (error) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getCuenta(id_acc) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM CUENTA WHERE ID_CUENTA = ?",
            [id_acc, id_rest, id_user],
            (error, resultado, campos) =>{
                if (error) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getCuentaNotAssigned(id_user) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM CUENTA c WHERE c.id_usuario = ? AND (c.fecha_apertura <= adddate(now(), interval '7 0' HOUR_MINUTE) AND c.fecha_apertura >= adddate(now(), interval '-7 0' HOUR_MINUTE)) AND (c.estado = 'OPEN' OR c.estado = 'CLOSED')",
            [id_user],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getCuentaUser(id_acc, id_rest, id_user) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM CUENTA WHERE ID_CUENTA = ? AND ID_RESTAURANTE = ? AND ID_USUARIO = ? AND ESTADO = 'OPEN' AND FECHA_APERTURA = (SELECT MAX(FECHA_APERTURA) FROM CUENTA)",
            [id_acc, id_rest, id_user],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getCuentaReciente(id_user) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM CUENTA c WHERE c.id_usuario = ? AND (c.fecha_apertura <= adddate(now(), interval '7 0' HOUR_MINUTE) AND c.fecha_apertura >= adddate(now(), interval '-7 0' HOUR_MINUTE)) AND (c.estado = 'OPEN' OR c.estado = 'CLOSED')",
            [id_user],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getAllCuentas(id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM CUENTA WHERE ID_RESTAURANTE = ? ",
            [id_rest],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    updateCuenta(id_cuenta, id_usuario) {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_MESA: this.ID_MESA,
                ID_USUARIO: this.ID_USUARIO,
                ID_RESTAURANTE: this.ID_RESTAURANTE,
                TOTAL_B: this.TOTAL_B,
                TOTAL_C: this.TOTAL_C,
                TOTAL_VAT: this.TOTAL_VAT,
                DESCRIPCION: this.DESCRIPCION,
                FECHA_APERTURA: this.FECHA_APERTURA,
                FECHA_CIERRE: this.FECHA_CIERRE,
                ESTADO: this.ESTADO
            };
            conexion.query("UPDATE CUENTA SET ? WHERE ID_CUENTA = ? AND ID_USUARIO = ? ",
            [datos, id_cuenta, id_usuario],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    deleteCuenta(id_cuenta, id_user) {
        return new Promise((resolve, reject) =>{
            conexion.query("DELETE FROM CUENTA WHERE ID_CUENTA = ? AND ID_USUARIO = ?",
            [id_cuenta, id_user],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }
}