const conexion = require('./../config/bdconfig');

module.exports = class Productos {
    constructor(productJSON) {
        this.ID_PRODUCTO = productJSON.id_producto || -1;
        this.ID_RESTAURANTE = productJSON.id_restaurante || null;
        this.NOMBRE = productJSON.nombre || null;
        this.IMAGEN = productJSON.imagen || 'images/products/defecto.jpg';
        this.PRECIO_UNIT = productJSON.precio_unit || null;
        this.DESCRIPCION = productJSON.descripcion || null;
        this.VAT = productJSON.vat || null;
        this.COSTE = productJSON.coste || null;
    }

    nuevoProducto() {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_RESTAURANTE: this.ID_RESTAURANTE,
                NOMBRE: this.NOMBRE,
                IMAGEN: this.IMAGEN,
                PRECIO_UNIT: this.PRECIO_UNIT,
                DESCRIPCION: this.DESCRIPCION,
                VAT: this.VAT
            };
            conexion.query("INSERT INTO PRODUCTOS SET ? ",
            datos, (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getProduct(id_order, id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM PRODUCTOS WHERE ID_PRODUCTO = ? AND ID_RESTAURANTE = ?",
            [id_order, id_rest],
            (error, resultado, campos) =>{

                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getAllProducts(id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM PRODUCTOS WHERE ID_RESTAURANTE = ?",
            [id_rest],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    updateProducto(id_order, id_rest) {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_RESTAURANTE: this.ID_RESTAURANTE,
                NOMBRE: this.NOMBRE,
                IMAGEN: this.IMAGEN,
                PRECIO_UNIT: this.PRECIO_UNIT,
                DESCRIPCION: this.DESCRIPCION,
                VAT: this.VAT
            };
            conexion.query("UPDATE PRODUCTOS SET ? WHERE ID_PRODUCTO = ? AND ID_RESTAURANTE = ? ",
            [datos, id_order, id_rest],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static deleteProducto(id_order, id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("DELETE FROM PRODUCTOS WHERE ID_PRODUCTO = ? AND ID_RESTAURANTE = ?",
            [id_order, id_rest],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }
}