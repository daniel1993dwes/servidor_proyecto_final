const conexion = require('./../config/bdconfig');
const Token = require('../token/token');
const md5 = require('md5');

module.exports = class User {
    constructor(usuarioJSON) {
        this.ID_USUARIO = usuarioJSON.id_usuario || -1;
        this.ID_RESTAURANTE = usuarioJSON.id_restaurante || 0;
        this.NOMBRE = usuarioJSON.nombre || 'ERROR';
        this.PASSWORD = usuarioJSON.password || null;
        this.TELEFONO = usuarioJSON.telefono || null;
        this.ID_MESA = usuarioJSON.id_mesa || 0;
        this.ROLE = usuarioJSON.role || 'ROLE_USER';
    }

    static validarUsuario(usuarioJSON) {
        return new Promise((resolve, reject) =>{
            let passEncrypt = md5(usuarioJSON.password + '');
            conexion.query("SELECT * FROM USUARIOS u WHERE NOMBRE = TRIM(?) AND PASSWORD = TRIM(?)",
            [usuarioJSON.nombre, passEncrypt],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static buscarUsuarioPorNombre(nombre) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM USUARIOS u WHERE NOMBRE LIKE ?",
            [nombre],
            (error, resultado, campos) =>{
                if (error) return reject(error);
                else return resolve(resultado);
            })
        })
    }
    
    static validarUsuarioRestaurante(ID_USER, ID_REST) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT COUNT(*) AS CANTIDAD FROM USUARIOS WHERE ID_USUARIO = ? AND ID_RESTAURANTE = ? AND ROLE = 'ROLE_ADMIN'",
            [ID_USER, ID_REST],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getUser(id_user) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM USUARIOS WHERE ID_USUARIO = ?",
            [id_user],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getAllUsers(ROLE_USER) {
        return new Promise((resolve, reject) =>{
            if(ROLE_USER == 'ROLE_SUPER_ADMIN') {
                conexion.query("SELECT * FROM USUARIOS",
                (error, resultado, campos) =>{

                    if (error || resultado.length == 0) return reject(error);
                    else return resolve(resultado[0]);
                })
            }
            else
            {
                return reject('No tienes permisos suficientes para el metodo getAllUsers');
            }
        })
    }

    nuevoUsuario() {
        return new Promise((resolve, reject) =>{
            let passEncrypt = md5(this.PASSWORD + '');
            let datos = {
                ID_RESTAURANTE: this.ID_RESTAURANTE,
                NOMBRE: this.NOMBRE,
                PASSWORD: passEncrypt,
                TELEFONO: this.TELEFONO,
                ID_MESA: this.ID_MESA,
                ROLE: this.ROLE
            };
            conexion.query("INSERT INTO USUARIOS SET ? ",
            datos, (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    updateUsuario(id) {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_RESTAURANTE: this.ID_RESTAURANTE,
                NOMBRE: this.NOMBRE,
                PASSWORD: this.PASSWORD,
                TELEFONO: this.TELEFONO,
                ID_MESA: this.ID_MESA,
                ROLE: this.ROLE
            };
            conexion.query("UPDATE USUARIOS SET ? WHERE ID_USUARIO = ? ",
            [datos, id],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    deleteUsuario(id) {
        return new Promise((resolve, reject) =>{
            conexion.query("DELETE FROM USUARIOS WHERE ID_USUARIO = ? ",
            [id],
            (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }
}