const conexion = require('./../config/bdconfig');

module.exports = class Restaurante {
    constructor(restJSON) {
        this.ID_RESTAURANTE = restJSON.id_restaurante || -1;
        this.ID_USUARIO = restJSON.id_usuario || null;
        this.NOMBRE = restJSON.nombre || null;
        this.TELEFONO = restJSON.telefono || null;
        this.IMAGEN = restJSON.imagen || null;
        this.DIRECCION = restJSON.direccion || null;
        this.DESCRIPCION = restJSON.descripcion || null;
        this.TIPOS_COMIDA = restJSON.tipos_comida || null;
    }

    nuevoRestaurante() {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_USUARIO: this.ID_USUARIO,
                NOMBRE: this.NOMBRE,
                TELEFONO: this.TELEFONO,
                IMAGEN: this.IMAGEN,
                DIRECCION: this.DIRECCION,
                DESCRIPCION: this.DESCRIPCION,
                TIPOS_COMIDA: this.TIPOS_COMIDA
            };
            conexion.query("INSERT INTO RESTAURANTES SET ? ",
            datos, (error, resultado, campos) =>{
                if (error || resultado.affectedRows == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getRestaurante(id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM RESTAURANTES WHERE ID_RESTAURANTE = ? ",
            [id_rest],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getAllRestaurantes(id_user) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM RESTAURANTES WHERE ID_USUARIO = ?",
            [id_user],
            (error, resultado, campos) =>{
                if (error) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    /**
     * Debe venir del cliente con %palabra% para la busqueda, si no, como null
     * @param {search, direccion} objetoBusqueda 
     */
    static getAllRestaurantesCoincidentes(objetoBusqueda) {
        return new Promise((resolve, reject) =>{
            let parametros = [];
            let sql = "SELECT * FROM RESTAURANTES R WHERE 1 = 1 ";
            if (objetoBusqueda.search!= null)
            {
                parametros.push(objetoBusqueda.search);
                parametros.push(objetoBusqueda.search);
                sql += "AND UPPER(R.NOMBRE) LIKE UPPER(?) OR UPPER(R.DESCRIPCION) LIKE UPPER(?)";
            }
            else
            {
                objetoBusqueda.search = '';
            }
            if (objetoBusqueda.direccion)
            {
                parametros.push(objetoBusqueda.direccion);
                sql += "AND UPPER(R.DIRECCION) LIKE UPPER(?)"
            }
            else
            {
                objetoBusqueda.direccion = '';
            }
            conexion.query(sql,
            parametros,
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    updateRestaurante(id_rest, id_user, ROLE) {
        return new Promise((resolve, reject) =>{
            let datos = {
                ID_USUARIO: this.ID_USUARIO,
                NOMBRE: this.NOMBRE,
                TELEFONO: this.TELEFONO,
                IMAGEN: this.IMAGEN,
                DIRECCION: this.DIRECCION,
                DESCRIPCION: this.DESCRIPCION,
                TIPOS_COMIDA: this.TIPOS_COMIDA
            };
            if(ROLE == 'ROLE_ADMIN')
            {
                conexion.query("UPDATE RESTAURANTES SET ? WHERE ID_RESTAURANTE = ? AND ID_USUARIO = ?",
                [datos, id_rest, id_user],
                (error, resultado, campos) =>{
                    if (error || resultado.affectedRows == 0) return reject(error);
                    else return resolve(resultado);
                })
            }
            else
            {
                return reject('No tienes permisos suficientes para esa accion');
            }
        })
    }

    static deleteRestaurante(id_rest, id_user, ROLE) {
        return new Promise((resolve, reject) =>{
            if(ROLE == 'ROLE_ADMIN')
            {
                conexion.query("DELETE FROM RESTAURANTES WHERE ID_RESTAURANTE = ? AND ID_USUARIO = ? ",
                [id_rest, id_user],
                (error, resultado, campos) =>{
                    if (error || resultado.affectedRows == 0) return reject(error);
                    else return resolve(resultado);
                })
            }
            else
            {
                return reject('No tienes permisos suficientes para esa accion');
            }   
        })
    }
}