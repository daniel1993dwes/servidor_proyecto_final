const conexion = require('./../config/bdconfig');

module.exports = class Mesas {
    constructor(mesasJSON) {
        this.ID_MESA = mesasJSON.id_mesa || -1;
        this.NUM_MESA = mesasJSON.num_mesa || null;
        this.CANT_USUARIOS = mesasJSON.cant_usuarios|| 0;
        this.PASSWORD = mesasJSON.password || null;
        this.OCUPADA = mesasJSON.ocupada || false;
        this.ID_RESTAURANTE = mesasJSON.id_restaurante || null;
    }

    nuevaMesa(role) {
        return new Promise((resolve, reject) =>{
            let datos = {
                NUM_MESA: this.NUM_MESA,
                CANT_USUARIOS: this.CANT_USUARIOS,
                PASSWORD: this.PASSWORD,
                OCUPADA: this.OCUPADA,
                ID_RESTAURANTE: this.ID_RESTAURANTE
            };
            if (role == 'ROLE_ADMIN') {
                conexion.query("INSERT INTO MESAS SET ? ",
                datos,
                (error, resultado, campos) =>{
                    if (error || resultado.affectedRows == 0) return reject(error);
                    else return resolve(resultado);
                })
            }
            else
            {
                return reject('No tienes permisos suficientes');
            }
            
        })
    }

    static getMesaPorId(id_mesa, id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM MESAS WHERE ID_MESA = ? AND ID_RESTAURANTE = ?",
            [id_mesa, id_rest],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0]);
            })
        })
    }

    static getMesaRestaurantePassword(password, id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM MESAS WHERE PASSWORD LIKE ? AND ID_RESTAURANTE = ?",
            [password, id_rest],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getAllMesas(id_rest) {
        return new Promise((resolve, reject) =>{
            conexion.query("SELECT * FROM MESAS WHERE ID_RESTAURANTE = ?",
            [id_rest],
            (error, resultado, campos) =>{
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado);
            })
        })
    }

    static getMesaDesocupada(password) {
        return new Promise((resolve, reject) => {
            conexion.query("SELECT * FROM MESAS WHERE OCUPADA = FALSE AND PASSWORD LIKE ? ",
            [password],
            (error, resultado, campos) => {
                if (error || resultado.length == 0) return reject(error);
                else return resolve(resultado[0])
            })
        })
    }

    updateMesa(role, id) {
        return new Promise((resolve, reject) =>{
            let datos = {
                NUM_MESA: this.NUM_MESA,
                CANT_USUARIOS: this.CANT_USUARIOS,
                PASSWORD: this.PASSWORD,
                OCUPADA: this.OCUPADA,
                ID_RESTAURANTE: this.ID_RESTAURANTE
            };
            if (role == 'ROLE_ADMIN') {
                conexion.query("UPDATE MESAS SET ? WHERE ID_MESA = ? ",
                [datos, id],
                (error, resultado, campos) =>{
                    if (error || resultado.affectedRows == 0) return reject(error);
                    else return resolve(resultado);
                })
            }
            else
            {
                return reject('No tienes permisos suficientes');
            }
        })
    }

    deleteMesa(role, id) {
        return new Promise((resolve, reject) =>{
            if (role == 'ROLE_ADMIN')
            {
                conexion.query("DELETE FROM MESAS WHERE ID_MESA = ? ",
                [id],
                (error, resultado, campos) =>{
                    if (error || resultado.affectedRows == 0) return reject(error);
                    else return resolve(resultado);
                })
            }
            else
            {
                return reject('No tienes permisos suficientes');
            }
        })
    }
}