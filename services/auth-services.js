/*const request = require('request');
const ImageDownloader = require('image-downloader');
const constant = require('./../constants/constants');
const Usuario = require('../models/user');
const Token = require('../token/token');

let time = constant.timeStamp;

module.exports = class AuthService {
    static getProfileGoogle(token) {
        return new Promise((resolve, reject) =>{
            request.get('https://www.googleapis.com/plus/v1/people/me?access_token=' + token)
            .on('response', (res) => {
                let body = '';
                res.on('data', (chunk) => {
                body+=chunk;
                }).on('end', () => {
                let datos =JSON.parse(body);
                return resolve(this.guardarUsuarioGoogle(datos, datos.image.url));
                })
            })
        })
    }

    static guardarUsuarioGoogle(datos, imageUrl) {
        const options = {
            url: imageUrl,
            dest: './images/users/' + time + '.jpg'
        }

        let imagen = options.dest.split('/');
            let usuario = new Usuario({
                name: datos.nickname,
                email: datos.emails[0].value,
                avatar: time + '.jpg'
            })

        return Usuario.validarUsuarioOAuth(usuario).then(result => {
            return result;
        }).catch(error => {
            return ImageDownloader.image(options).then(({filename, image}) =>{
                return usuario.nuevoUsuario().then(result =>{
                    return Token.generarToken(result.insertId);
                }).catch(error => {
                    console.log('Error al crear el usuario de Google: ' + error);
                })
            }).catch(error =>{
                console.log('Error al subir la imagen de usuario de Google: ' + error);
            })
        })
    }

    static getProfileFacebook(token) {
        return new Promise((resolve, reject) =>{
            request.get('https://graph.facebook.com/v3.2/me?fields=id,name,email,picture&access_token=' + token)
            .on('response', (res) => {
                let body = '';
                res.on('data', (chunk) => {
                body+=chunk;
                }).on('end', () => {
                let datos =JSON.parse(body);
                return resolve(this.guardarUsuarioFacebook(datos, datos.picture.data.url));
                })
            })
        })
    }

    static guardarUsuarioFacebook(datos, imageUrl) {
        const options = {
            url: imageUrl,
            dest: './images/users/' + time + '.jpg'
        }

        let imagen = options.dest.split('/');
            let usuario = new Usuario({
                name: datos.name,
                email: datos.email,
                avatar: time + '.jpg'
            })

        return Usuario.validarUsuarioOAuth(usuario).then(result => {
            return result;
        }).catch(error => {
            return ImageDownloader.image(options).then(({filename, image}) =>{
                return usuario.nuevoUsuario().then(result =>{
                    return Token.generarToken(result.insertId);
                }).catch(error => {
                    console.log('Error al crear el usuario de Facebook: ' + error);
                })
            }).catch(error =>{
                console.log('Error al subir la imagen de usuario de Facebook: ' + error);
            })
        })
    }
}*/