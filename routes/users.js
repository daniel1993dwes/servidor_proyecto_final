const express = require('express');
const router = express.Router();
const Usuario = require('../models/user');
const Mesas = require('../models/mesas');
const Cuentas = require('../models/cuentas');
const Token = require('../token/token');
const constant = require('./../constants/constants');

let time = constant.timeStamp;

router.get("/me",
(req, res) => {
    Usuario.getUser(req.user.id, req.user.rest).then(result => {
        res.status(200).send(result);
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    });
});

router.get("/:id",
(req, res) => {
    let userId = req.params.id;
    Usuario.getUser(userId).then(result => {
        res.status(200).send({user: result, token: Token.generarToken(req.user.id, req.user.role, req.user.rest)});
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    });
});

router.put("/me",
(req, res) => {
    let usuario = new Usuario(req.body);
    usuario.updateUsuario(req.user.id).then(result => {
        res.status(200).send({ result: result, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Esa cuenta no existe', errorEspec: e1 });
    })
});

router.put("/:id",
(req, res) => {
    if (req.user.role == 'ROLE_ADMIN')
    {
        let usuario = new Usuario(req.body);
        usuario.updateUsuario(req.params.id).then(result => {
            res.status(200).send({ result: result, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e => {
            res.status(400).send(e);
        })
    }
    else
    {
        res.status(401).send({ error: true, mensajeError: 'Acceso no autorizado' })
    }
    
});

module.exports = router;