const express = require('express');
const router = express.Router();
const passport = require('passport');
const Test = require('./../models/pruebas');

router.post("",
(req, res) => {
    Test.TransactionCreator().then(response => {
        res.status(200).send(response);
    }).catch(error => {
        res.status(400).send(error);
    });
});

module.exports = router;