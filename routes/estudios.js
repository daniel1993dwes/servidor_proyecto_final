const express = require('express');
const router = express.Router();
const Estudio = require('./../models/estudios');
const Token = require('../token/token');

router.post("/ventas",
(req, res) => {
    Estudio.getVentasFecha(req.body.fecha1, req.body.fecha2).then(r1 => {
        res.status(200).send({result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'No tienes cuentas abiertas', errorEspec: e1 });
    })
});

router.post("/ventas-stats",
(req, res) => {
    Estudio.getVentasHechosBfos(req.body.fecha1, req.body.fecha2).then(r1 => {
        res.status(200).send({result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'No tienes cuentas abiertas', errorEspec: e1 });
    })
});

router.post("/ventas-stats",
(req, res) => {
    Estudio.getVentasHechosBfos(req.body.fecha1, req.body.fecha2).then(r1 => {
        res.status(200).send({result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'No tienes cuentas abiertas', errorEspec: e1 });
    })
});

router.post("/productos-stats",
(req, res) => {
    Estudio.getProductosHechosAux(req.body.fecha1, req.body.fecha2).then(r1 => {
        res.status(200).send({result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'No tienes cuentas abiertas', errorEspec: e1 });
    })
});

module.exports = router;