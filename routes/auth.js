const express = require("express");
const router = express.Router();
const User = require("./../models/user");
const Cuentas = require("./../models/cuentas");
const Token = require('./../token/token');

router.post("/login", (req, res) => {
  User.validarUsuario({ nombre: req.body.nombre, password: req.body.password }).then(resultado => {
        Cuentas.getCuentaNotAssigned(resultado.id_usuario).then(result => {
          User.getUser(resultado.id_usuario).then(r => {
              let usuario = new User(r);
              if (r.role == 'ROLE_ADMIN' || r.role == 'ROLE_SUPER_ADMIN') {
                usuario.id_mesa = 0;
              } else {
                usuario.ID_MESA = result.id_mesa;
                usuario.ID_RESTAURANTE = result.id_restaurante;
              }
              usuario.updateUsuario(resultado.id_usuario).then(r1 => {
                if (resultado) res.status(200).send({ result: resultado, accessToken: Token.generarToken(resultado.id_usuario, resultado.role, result.id_restaurante) });
              }).catch(e => {
                res.status(400).send({ error: true, mensajeError: 'Ese usuario no existe para actualizar', errorEspec: e })
              })
          }).catch(e1 => {
              res.status(400).send({ error: true, mensajeError: 'Ese usuario no existe en la busqueda', errorEspec: e1 })
          })
      }).catch(e2 => {
        if (resultado) res.status(200).send({ result: resultado, accessToken: Token.generarToken(resultado.id_usuario, resultado.role, resultado.id_restaurante) });
      })
    })
    .catch(error => {
      res.status(400).send({ ok: false, mensajeError: "Usuario incorrecto", mensajeConcreto: error });
    });
});

router.post("/nombre",
(req, res) => {
    User.buscarUsuarioPorNombre(req.body.nombre).then(result => {
        res.status(200).send(result);
    }).catch(error => {
        res.status(400).send({error: true, mensajeError: 'Bad Request'});
    });
});

// router.post("/register", (req, res) => {
//   let usuario = new User(req.body);
//     Mesas.getMesaDesocupada(req.body.password_mesa).then(ocu => {
//         usuario.ID_MESA = ocu.id_mesa;
//         usuario.nuevoUsuario().then(r1=> {
//             res.status(200).send({ result: r1 });
//         }).catch(e => {
//             res.status(400).send(e);
//         })
//     }).catch(errorOcu => {
//         res.status(400).send({ error: true, mensajeError: 'Mesa ocupada, no puedes cambiarte de mesa', errorEspec: errorOcu })
//     });
// });

router.post("/register", (req, res) => {
  let usuario = new User(req.body);
    usuario.nuevoUsuario().then(r1=> {
        res.status(200).send({ result: r1 });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Bad Request', errorEspec: e1 });
    })
});

router.get("/validate",
 (req, res) => {
   let result = Token.validarToken(req.headers.authorization);
   if (result) {
      res.status(200).send({ ok: true });
   } else {
      res.status(401).send({ ok: false });
   }
  
});

/*
router.post("/google", async (req, res) => {
  let token = req.body.token;
  AuthService.getProfileGoogle(token).then(result => {
    res.status(200).send({ token: result })
  }).catch(error =>{
    res.status(401).send({error: true, mensajeError: 'Unauthorized'});
  });
});

router.post("/facebook", async (req, res) => {
  let token = req.body.token;
  AuthService.getProfileFacebook(token).then(result => {
    res.status(200).send({ token: result })
  }).catch(error =>{
    res.status(401).send({error: true, mensajeError: 'Unauthorized'});
  });
});
*/
module.exports = router;
