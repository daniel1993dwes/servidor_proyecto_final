const express = require('express');
const router = express.Router();
const passport = require('passport');
const Restaurante = require('./../models/restaurantes');
const fs = require('fs');
const constant = require('./../constants/constants');
const Token = require('./../token/token');
const sharp = require('sharp');

let time = constant.timeStamp;

router.post("",
(req, res) => {
    let restaurante = new Restaurante(req.body);
    restaurante.IMAGEN = './images/restaurants/' + time + '.jpg';
    restaurante.ID_USUARIO = req.user.id;

    restaurante.nuevoRestaurante().then(async r1 => {
        let archivo = Buffer.from(req.body.imagen, 'base64');
        sharp(archivo)
            .resize(950, 600)
            .toFile(restaurante.IMAGEN, (err, info) => {
                if(err) throw err;
                console.log('Archivo subido');
            })
        // await fs.writeFile(restaurante.IMAGEN, archivo, 'base64', (err) => {
        // if (err) throw err;
        //     console.log('Archivo de restaurante subido con exito');
        // });

        res.status(200).send({ result: r1, accessToken: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({error: true, mensajeError: 'Bad Request', errorEspec: e1});
    })
});

router.get("",
(req, res) => {

    Restaurante.getAllRestaurantes(req.user.id).then(result => {
        res.status(200).send({ result: result, accessToken: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({error: true, mensajeError: 'Bad Request', errorEspec: e1});
    })
});

router.get("/filtrar",
(req, res) => {
    Restaurante.getAllRestaurantesCoincidentes(req.body).then(result => {
        res.status(200).send({ result: result, accessToken: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(404).send({error: true, mensajeError: 'Not Found Filter', errorEspec: e1});
    })
});

router.get("/me",
(req, res) => {
    Restaurante.getRestaurante(req.user.rest).then(r1 => {
        res.status(200).send({ result: r1, accessToken: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(404).send({error: true, mensajeError: 'Not found', errorEspec: e1});
    })
});

router.put("/:id",
(req, res) => {
    let restaurante = new Restaurante(req.body);
    restaurante.IMAGEN = './images/restaurants/' + time + '.jpg';
    Restaurante.getRestaurante(req.params.id, req.user.id).then(async r1 => {
        await fs.unlink(r1.imagen, (err) => {
            if (err) throw err;
            console.log('Antiguo archivo borrado');
        })
        restaurante.updateRestaurante(req.params.id, req.user.id, req.user.role).then(async result => {
            let archivo = Buffer.from(req.body.imagen, 'base64');
            sharp(archivo)
                .resize(950, 600)
                .toFile(restaurante.IMAGEN, (err, info) => {
                    if(err) throw err;
                    console.log('Archivo subido');
                })
    
            res.status(200).send({ result: result, accessToken: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e1 => {
            res.status(404).send({error: true, mensajeError: 'Not Found', errorEspec: e1});
        })
    }).catch(e1 => {
        res.status(404).send({error: true, mensajeError: 'Not Found', errorEspec: e1});
    })
});

router.delete("/:id",
(req, res) => {
    Restaurante.getRestaurante(req.params.id, req.user.id).then(async r1 => {
        await fs.unlink(r1.imagen, (err) => {
            if (err) throw err;
            console.log('Antiguo archivo borrado');
        })
        Restaurante.deleteRestaurante(req.params.id, req.user.id, req.user.role).then(r2 => {
            res.status(200).send({ result: r2, accessToken: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e1 => {
            res.status(404).send({error: true, mensajeError: 'Not Found', errorEspec: e1});
        })
    }).catch(e2 => {
        res.status(404).send({error: true, mensajeError: 'Not Found', errorEspec: e2});
    })
});

module.exports = router;