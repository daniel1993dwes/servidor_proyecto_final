const express = require('express');
const router = express.Router();
const Cuentas = require('./../models/cuentas');
const Mesa = require('./../models/mesas');
const Token = require('../token/token');
const Usuario = require('./../models/user');
const Restaurante = require('./../models/restaurantes');
const Pedido = require('./../models/pedidos');

router.post("",
(req, res) => {
    let pedido = new Pedido(req.body);
    pedido.nuevoPedido().then(r1 => {
        Pedido.getPedido(req.body.id_cuenta, r1).then(r2 => {
            res.status(200).send({ result: r2, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e2 => {
            res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e2 });
        })
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e1 });
    })
});

router.get("/:id",
(req, res) => {
    Pedido.getAllPedidos(req.params.id).then(r1 => {

        res.status(200).send({ result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e2 => {
        res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e2 });
    })
});

router.get("/:id_account/:id_order",
(req, res) => {
    Pedido.getPedido(req.params.id_account, req.params.id_order).then(r2 => {
        res.status(200).send({ result: r2, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e2 => {
        res.status(400).send({ error: true, mensajeError: 'Ese pedido no existe', errorEspec: e2 });
    })
});

/**
 * Debe coincidir la cuenta con el id del producto para poder actualizar
 */
router.put("/:id",
(req, res) => {
    let pedido = new Pedido(req.body);
    pedido.updatePedido(req.params.id, req.body.id_cuenta).then(r1 => {
        res.status(200).send({ result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e1 });
    })
});

router.delete("/:id_order/:id_cuenta",
(req, res) => {
    Pedido.deletePedido(req.params.id_order, req.params.id_cuenta).then(r1 => {
        res.status(200).send({ result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e1 });
    })
});

module.exports = router;