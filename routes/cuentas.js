const express = require('express');
const router = express.Router();
const Cuentas = require('./../models/cuentas');
const Mesas = require('./../models/mesas');
const Usuario = require('./../models/user');
const Token = require('../token/token');
const Restaurante = require('../models/restaurantes');

router.post("",
(req, res) => {
    let cuenta = new Cuentas(req.body);
    cuenta.nuevaCuenta().then(result => {
        res.status(200).send({ result: result, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e => {
        res.status(400).send({ error: true, mensajeError: ' no puedes cambiarte de mesa', errorEspec: e });
    })
});

router.delete("/:id",
(req, res) => {
    Cuentas.getCuenta(req.params.id).then(r1 => {
        let cuenta = new Cuentas(r1);
        cuenta.deleteCuenta(cuenta.ID_CUENTA, req.user.id).then(r2 => {
            res.status(200).send({ result: r2, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e2 => {
            res.status(404).send({ error: true, mensajeError: 'No existe ese registro', errorEspec: e2 });
        })
    }).catch(e1 => {
        res.status(404).send({ error: true, mensajeError: 'No existe ese registro', errorEspec: e1 });
    })
});

router.get("/not-assigned",
(req, res) => {
    if (req.user.role == 'ROLE_ADMIN' || req.user.role == 'ROLE_SUPER_ADMIN') {
        res.status(200).send({ result: 'ADMIN', token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        return;
    }
    Cuentas.getCuentaNotAssigned(req.user.id).then(result => {
        Usuario.getUser(req.user.id).then(r => {
            let usuario = new Usuario(r);
                usuario.ID_MESA = result.id_mesa;
                usuario.ID_RESTAURANTE = result.id_restaurante;
                usuario.updateUsuario(req.user.id).then(r1 => {
                    res.status(200).send({ result: result, token: Token.generarToken(req.user.id, req.user.role, result.id_restaurante) });
                }).catch(e => {
                    res.status(400).send({ error: true, mensajeError: 'Ese usuario no existe', errorEspec: e });
                })
        }).catch(e1 => {
            res.status(400).send({ error: true, mensajeError: 'Ese usuario no existe', errorEspec: e1 })
        })
    }).catch(e2 => {
        res.status(400).send({ error: true, mensajeError: 'No tienes cuentas asignadas', errorEspec: e2 })
    })
});

router.get("/me",
(req, res) => {
    Cuentas.getCuentaNotAssigned(req.user.id).then(r1 => {
        res.status(200).send({result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'No tienes cuentas abiertas', errorEspec: e1 });
    })
});

router.get("/pagar/:id",
(req, res) => {
    Cuentas.getCuentaClosedId(req.params.id, req.user.rest, req.user.id).then(r => {
        let cuenta = new Cuentas(r);
        cuenta.FECHA_CIERRE = new Date();
        cuenta.ESTADO = 'PAID';
        cuenta.ID_USUARIO = req.user.id;
        cuenta.updateCuenta(req.params.id, req.user.id).then(result => {
            res.status(200).send({ result: result, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e => {
            res.status(400).send(e);
        })
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Esa cuenta no existe', errorEspec: e1 })
    })
});

router.get("/cerrar/:id",
(req, res) => {
    Cuentas.getCuentaUser(req.params.id, req.user.rest, req.user.id).then(r => {
        let cuenta = new Cuentas(r);
        let hoy = new Date();
        cuenta.FECHA_CIERRE = cuenta.FECHA_CIERRE = hoy.getDate() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getFullYear() + ' ' + hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
        cuenta.FECHA_CIERRE = new Date();
        cuenta.ESTADO = 'CLOSED';
        cuenta.ID_USUARIO = req.user.id;
        cuenta.updateCuenta(req.params.id, req.user.id).then(result => {
            res.status(200).send({ result: result, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e => {
            res.status(400).send(e);
        })
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Esa cuenta no existe', errorEspec: e1 })
    })
});

router.get("/abrir/:id",
(req, res) => {
    Cuentas.getCuentaClosedId(req.user.rest, req.user.id).then(r => {
        let cuenta = new Cuentas(r);
        cuenta.ESTADO = 'OPEN';
        cuenta.FECHA_CIERRE = null;
        cuenta.ID_USUARIO = req.user.id;
        cuenta.updateCuenta(req.params.id, req.user.id).then(result => {
            res.status(200).send({ result: result, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e => {
            res.status(400).send(e);
        })
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Esa cuenta no existe', errorEspec: e1 })
    })
});


router.get("/restaurante/mesa/:id",
(req, res) => {
    Usuario.validarUsuarioRestaurante(req.user.id, req.user.rest).then(r0 => {
        if (r0.CANTIDAD > 0)
        {
            Cuentas.getAllCuentasCerradasMesa(req.params.id, req.user.rest).then(async r1 => {
                try {
                    let lista = r1.map(async ele => {
                        return Restaurante.getRestaurante(ele.id_restaurante, ele.id_usuario).then(r2 => {
                            ele.id_restaurante = r2;
                            return ele;
                        });
                    });
                    let listaCuentasRestaurantes = await Promise.all(lista);
                    res.status(200).send({result: listaCuentasRestaurantes, token: Token.generarToken(req.user.id, req.user.role, req.user.rest)})
                }
                catch(e) {
                    res.status(400).send({ error: true, mensajeError: 'No se ejecutó la promesa', errorEspec: e1 })
                }
            }).catch(e1 => {
                res.status(400).send({ error: true, mensajeError: 'Esa cuenta no existe', errorEspec: e1 })
            })
        }
        else
        {
            res.status(400).send({ error: true, mensajeError: 'Ese restaurante no existe o no tienes permisos suficientes', errorEspec: e1 })
        }
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Ese restaurante no existe', errorEspec: e1 })
    })
});

router.put("/mesa",
(req, res) => {
    Cuentas.getCuentaReciente(req.user.id).then(r => {
        let cuenta = new Cuentas(r);
        Mesas.getMesaDesocupada(req.body.password).then(ocu => {
            cuenta.ID_MESA = ocu.id_mesa;
            cuenta.ID_RESTAURANTE = ocu.id_restaurante;
            cuenta.ID_USUARIO = req.user.id;
            cuenta.TOTAL = 0;
            cuenta.updateCuenta(cuenta.ID_CUENTA, req.user.id).then(result => {
                Usuario.getUser(req.user.id, 0).then(r => {
                    let usuario = new Usuario(r);
                        usuario.ID_USUARIO = req.user.id;
                        usuario.ID_MESA = cuenta.ID_MESA;
                        usuario.ID_RESTAURANTE = cuenta.ID_RESTAURANTE;
                        usuario.updateUsuario(req.user.id).then(result => {
                            res.status(200).send({ result: usuario, token: Token.generarToken(req.user.id, req.user.role, cuenta.ID_RESTAURANTE) });
                        }).catch(e => {
                            res.status(400).send({ error: true, mensajeError: 'Error al actualizar al usuario', errorEspec: e });
                        })
                }).catch(e1 => {
                    cuenta.ID_RESTAURANTE = 0;
                    cuenta.ID_MESA = 0;
                    cuenta.updateCuenta(cuenta.ID_CUENTA, req.user.id).then(res2 => {
                        res.status(400).send({ error: true, mensajeError: 'Ese usuario ya tiene un restaurante asignado', errorEspec: e1 })
                    }).catch(error2 => {
                        res.status(400).send({ error: true, mensajeError: 'Imposible actualizar la cuenta', errorEspec: error2 })
                    })
                })
            }).catch(e2 => {
                res.status(400).send({ error: true, mensajeError: 'Error al actualizar la cuenta', errorEspec: e2.result });
            })
        }).catch(e3 => {
            res.status(400).send({ error: true, mensajeError: 'Mesa ocupada, no puedes cambiarte de mesa', errorEspec: e3 })
        });
    }).catch(e4 => {
        Mesas.getMesaDesocupada(req.body.password).then(ocu => {
            let cuenta = new Cuentas(ocu);
            cuenta.ID_MESA = ocu.id_mesa;
            cuenta.ID_RESTAURANTE = ocu.id_restaurante;
            cuenta.ID_USUARIO = req.user.id;
            cuenta.FECHA_APERTURA = new Date();
            cuenta.TOTAL = 0;
            cuenta.nuevaCuenta().then(result => {
                Usuario.getUser(req.user.id).then(r => {
                    let usuario = new Usuario(r);
                        usuario.ID_USUARIO = req.user.id;
                        usuario.ID_MESA = cuenta.ID_MESA;
                        usuario.ID_RESTAURANTE = cuenta.ID_RESTAURANTE;
                        usuario.updateUsuario(req.user.id).then(result => {
                            res.status(200).send({ result: usuario, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
                        }).catch(e8 => {
                            res.status(400).send({ error: true, mensajeError: 'Error al actualizar al usuario', errorEspec: e8 });
                        })
                    }).catch(e5 => {
                        res.status(400).send({ error: true, mensajeError: 'Ese usuario no existe', errorEspec: e5 })
                    })
                }).catch(e6 => {
                    res.status(400).send({ error: true, mensajeError: 'Error al añadir la cuenta', errorEspec: e6.result });
                })
        }).catch(e7 => {
            res.status(400).send({ error: true, mensajeError: 'Mesa ocupada, no puedes cambiarte de mesa', errorEspec: e7 })
        })
    });
});

router.get("/:id",
(req, res) => {
    Cuentas.getCuentaClosedId(req.user.rest, req.user.id).then(r1 => {
        Usuario.getUser(r1.id_usuario).then(r2 => {
            r1.id_usuario = r2;
            res.status(200).send({restaurante: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e2 => {
            res.status(400).send({ error: true, mensajeError: 'Esa cuenta no existe', errorEspec: e2 });
        })
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Esa cuenta no existe', errorEspec: e1 });
    })
});

module.exports = router;