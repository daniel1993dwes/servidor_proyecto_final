const express = require('express');
const router = express.Router();
const Cuentas = require('./../models/cuentas');
const Mesa = require('./../models/mesas');
const Token = require('../token/token');
const Usuario = require('./../models/user');
const Restaurante = require('./../models/restaurantes');

router.post("",
(req, res) => {
    let mesa = new Mesa(req.body);
    mesa.nuevaMesa(req.user.role).then(r1 => {
        res.status(200).send({ result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'No se puede insertar esa mesa', errorEspec: e1 });
    })
});

router.get("/restaurant/:id",
(req, res) => {
    Mesa.getAllMesas(req.params.id).then(async r1 => {
        try {
            let arry = r1.map(async ele => {
                    return Restaurante.getRestaurante(ele.id_restaurante, req.user.id).then(r2 => {
                    ele.id_restaurante = r2;
                    return ele;
                });
            });
            let otr = await Promise.all(arry);
            res.status(200).send({ result: otr, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }
        catch(e) {
            res.status(400).send({ error: true, mensajeError: 'No se ejecutó la promesa', errorEspec: e1 });
        }
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'No se puede listar las mesas', errorEspec: e1 });
    });
});

router.get("/:id",
(req, res) => {
    Mesa.getMesaPorId(req.params.id, req.user.rest).then(r1 => {
        Restaurante.getRestaurante(r1.id_restaurante, req.user.id).then(r2 => {
            r1.id_restaurante = r2;
            res.status(200).send({ result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        });
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'No se encuentra esa mesa', errorEspec: e1 });
    })
});

router.put("/:id",
(req, res) => {
    Mesa.getMesaRestaurantePassword(req.body.password, req.body.id_restaurante).then(r1 => {
        res.status(400).send({ error: true, mensajeError: 'Esa contraseña ya existe' });
    }).catch(e1 => {
        let mesa = new Mesa(req.body);
        mesa.updateMesa(req.user.role, req.params.id).then(r2 => {
            res.status(200).send({ result: r2, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e2 => {
            res.status(400).send({ error: true, mensajeError: 'No se puede actualizar esa mesa', errorEspec: e2 });
        })
    })
});

module.exports = router;