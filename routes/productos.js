const express = require('express');
const router = express.Router();
const Token = require('../token/token');
const Producto = require('./../models/productos');
const sharp = require('sharp');
const constant = require('./../constants/constants');
const fs = require('fs');

let time = constant.timeStamp;

router.post("",
(req, res) => {
    let producto = new Producto(req.body);
    producto.IMAGEN = './images/products/' + time + '.jpg';
    producto.nuevoProducto().then(r1 => {
        let archivo = Buffer.from(req.body.imagen, 'base64');
        sharp(archivo)
            .resize(400)
            .toFile(producto.IMAGEN, (err, info) => {
                if(err) throw err;
                console.log('Archivo subido');
            })
        Producto.getProduct(r1.insertId, req.user.rest).then(r2 => {
            res.status(200).send({ result: r2, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e2 => {
            res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e2 });
        })
    }).catch(e1 => {
        res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e1 });
    })
});

router.get("",
(req, res) => {
    Producto.getAllProducts(req.user.rest).then(r1 => {
        res.status(200).send({ result: r1, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e2 => {
        res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e2 });
    })
});

router.get("/:id",
(req, res) => {
    Producto.getProduct(req.params.id, req.user.rest).then(r2 => {
        res.status(200).send({ result: r2, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
    }).catch(e2 => {
        res.status(400).send({ error: true, mensajeError: 'Ese pedido no existe', errorEspec: e2 });
    })
});

router.put("/:id",
(req, res) => {
    let producto = new Producto(req.body);
    Producto.getProduct(req.params.id, req.user.rest).then(async r2 => {
        try {
            await fs.unlink(r2.imagen, (err) => {
                if (err) console.error(err);
                console.log('Antiguo archivo borrado');
            })
        } catch (e) {
            console.log(e);
        }
        producto.IMAGEN = './images/products/' + time + '.jpg';
        producto.updateProducto(req.params.id, req.user.rest).then(r1 => {
            try {
                let archivo = Buffer.from(req.body.imagen, 'base64');
                sharp(archivo)
                    .resize(950, 600)
                    .toFile(producto.IMAGEN, (err, info) => {
                        if(err) console.error(err);
                        console.log('Archivo subido');
                    })
            } catch(e) {
                console.error(e)
            }
            Producto.getProduct(req.params.id, req.user.rest).then(r2 => {
                res.status(200).send({ result: r2, token: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
            }).catch(e2 => {
                res.status(400).send({ error: true, mensajeError: 'Ese producto no existe', errorEspec: e2 });
            })
        }).catch(e1 => {
            res.status(400).send({ error: true, mensajeError: 'Ha habido un error, intentelo más tarde', errorEspec: e1 });
        })
    }).catch(e1 => {
        res.status(404).send({error: true, mensajeError: 'Not Found', errorEspec: e1});
    })
});

router.delete("/:id",
(req, res) => {
    Producto.getProduct(req.params.id, req.user.rest).then(async r1 => {
        await fs.unlink(r1.imagen, (err) => {
            if (err) throw err;
            console.log('Antiguo archivo borrado');
        })
        Producto.deleteProducto(req.params.id, req.user.rest).then(r2 => {
            res.status(200).send({ result: r2, accessToken: Token.generarToken(req.user.id, req.user.role, req.user.rest) });
        }).catch(e1 => {
            res.status(404).send({error: true, mensajeError: 'Not Found', errorEspec: e1});
        })
    }).catch(e1 => {
        res.status(404).send({error: true, mensajeError: 'Not Found', errorEspec: e1});
    })
});

module.exports = router;