-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2019 a las 14:42:58
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dw_easy_local`
--
CREATE DATABASE IF NOT EXISTS `dw_easy_local` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `dw_easy_local`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `SCN_LD_HC_DIARIO_AUX_ALL`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SCN_LD_HC_DIARIO_AUX_ALL` ()  NO SQL
BEGIN
INSERT INTO hc_diario_aux_all
	SELECT
NULL,
TOTAL_A_CUENTA_ALL,
FECHA_CARGA
FROM
(
 	SELECT
    NULL,
    SUM(C.TOTAL_A_CUENTA) AS TOTAL_A_CUENTA_ALL,
    NOW() AS FECHA_CARGA
    FROM
    HC_D_DIARIO_AUX C
    WHERE
    DATE_FORMAT(C.FECHA_CARGA, '%d/%m/%Y') = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%d/%m/%Y')
) R;
END$$

DROP PROCEDURE IF EXISTS `SCN_LD_HC_D_DIARIO_ALL`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SCN_LD_HC_D_DIARIO_ALL` ()  BEGIN
INSERT INTO hc_d_diario_all
	SELECT
NULL,
TOTAL_B,
TOTAL_C,
TOTAL_VAT,
MEDIA_HORAS,
FECHA_CARGA
FROM
(
 	SELECT
    NULL,
    SUM(C.TOTAL_B) AS TOTAL_B,
    SUM(C.TOTAL_C) AS TOTAL_C,
    SUM(C.TOTAL_VAT) AS TOTAL_VAT,
    HOUR(SUM(C.MEDIA_HORAS)/COUNT(C.MEDIA_HORAS)) AS MEDIA_HORAS,
    NOW() AS FECHA_CARGA
    FROM
    HC_D_DIARIO_CATALOGO C
    WHERE
    DATE_FORMAT(C.FECHA_ENTRADA, '%d/%m/%Y') = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%d/%m/%Y')
) R;
END$$

DROP PROCEDURE IF EXISTS `SCN_LD_HC_D_DIARIO_AUX`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SCN_LD_HC_D_DIARIO_AUX` ()  BEGIN
INSERT INTO HC_D_DIARIO_AUX
	SELECT
NULL,
CNT_TOTAL,
ID_PRODUCTO,
TOTAL_A_CUENTA,
FECHA_CARGA
FROM
(
 	SELECT
    NULL,
    COUNT(C.ID_PRODUCTO) AS CNT_TOTAL,
    C.ID_PRODUCTO AS ID_PRODUCTO,
    SUM(C.PRECIO_FINAL) AS TOTAL_A_CUENTA,
    NOW() AS FECHA_CARGA
    FROM
    EASY_LOCAL.PEDIDOS C
    WHERE
    DATE_FORMAT(C.FECHA, '%d/%m/%Y') = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%d/%m/%Y')
    GROUP BY C.ID_PRODUCTO
) R;
	CALL SCN_LD_HC_DIARIO_AUX_ALL;
END$$

DROP PROCEDURE IF EXISTS `SCN_LD_HC_D_DIARIO_CATALOGO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SCN_LD_HC_D_DIARIO_CATALOGO` ()  BEGIN
INSERT INTO hc_d_diario_catalogo
SELECT
NULL,
TOTAL_B,
TOTAL_C,
TOTAL_VAT,
MEDIA_HORAS,
CATEGORIA
FROM
(
    SELECT
    NULL,
    SUM(A.TOTAL_B) AS TOTAL_B,
    SUM(A.TOTAL_C) AS TOTAL_C,
    SUM(A.TOTAL_VAT) AS TOTAL_VAT,
    HOUR(SUM(TIMEDIFF(A.FECHA_CIERRE, A.FECHA_APERTURA))/COUNT(A.FECHA_APERTURA)) AS MEDIA_HORAS,
    (CASE
    WHEN HOUR(A.fecha_apertura)>=7 and HOUR(A.FECHA_APERTURA)<10 THEN 'A'
    WHEN HOUR(A.fecha_apertura)>=10 and HOUR(A.FECHA_APERTURA)<13 THEN 'B'
    WHEN HOUR(A.fecha_apertura)>=13 and HOUR(A.FECHA_APERTURA)<16 THEN 'C'
    WHEN HOUR(A.fecha_apertura)>=16 and HOUR(A.FECHA_APERTURA)<19 THEN 'D'
    WHEN HOUR(A.fecha_apertura)>=19 and HOUR(A.FECHA_APERTURA)<22 THEN 'E'
    WHEN HOUR(A.fecha_apertura)>=22 and HOUR(A.FECHA_APERTURA)<1 THEN 'F'
    END) AS CATEGORIA
FROM
    EASY_LOCAL.CUENTA A
WHERE
    DATE_FORMAT(A.FECHA_APERTURA, '%d/%m/%Y') = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%d/%m/%Y') AND A.ESTADO = 'PAID' GROUP BY CATEGORIA) R GROUP BY CATEGORIA;
    CALL SCN_LD_HC_D_DIARIO_ALL;
END$$

DROP PROCEDURE IF EXISTS `SCN_LD_HC_HIST_ALL`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SCN_LD_HC_HIST_ALL` ()  BEGIN
INSERT INTO HC_HIST_ALL
	SELECT
NULL,
FECHA_CARGA,
MEDIA_HORAS,
TOTAL_B,
TOTAL_C,
TOTAL_VAT
FROM
(
 	SELECT
    NULL,
    NOW() AS FECHA_CARGA,
	HOUR(SUM(C.MEDIA_HORAS)/COUNT(C.MEDIA_HORAS)) AS MEDIA_HORAS,
	SUM(C.TOTAL_B) AS TOTAL_B,
	SUM(C.TOTAL_C) AS TOTAL_C,
	SUM(C.TOTAL_VAT) AS TOTAL_VAT
    FROM
    HC_D_DIARIO_ALL C
    WHERE
    DATE_FORMAT(C.FECHA_CARGA, '%d/%m/%Y') = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%d/%m/%Y')
) R;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hc_diario_aux_all`
--

DROP TABLE IF EXISTS `hc_diario_aux_all`;
CREATE TABLE `hc_diario_aux_all` (
  `id_registro` int(11) NOT NULL,
  `total_a_cuenta_all` decimal(10,3) NOT NULL,
  `fecha_carga` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hc_d_diario_all`
--

DROP TABLE IF EXISTS `hc_d_diario_all`;
CREATE TABLE `hc_d_diario_all` (
  `id_registro` int(11) NOT NULL,
  `total_b` decimal(10,3) NOT NULL,
  `total_c` decimal(10,3) NOT NULL,
  `total_vat` decimal(10,3) NOT NULL,
  `media_horas` datetime NOT NULL,
  `fecha_carga` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hc_d_diario_aux`
--

DROP TABLE IF EXISTS `hc_d_diario_aux`;
CREATE TABLE `hc_d_diario_aux` (
  `id_registro` int(11) NOT NULL,
  `cnt_total` decimal(10,3) NOT NULL DEFAULT '0.000',
  `id_producto` int(11) NOT NULL,
  `total_a_cuenta` decimal(10,3) NOT NULL DEFAULT '0.000',
  `fecha_carga` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hc_d_diario_aux`
--

INSERT INTO `hc_d_diario_aux` (`id_registro`, `cnt_total`, `id_producto`, `total_a_cuenta`, `fecha_carga`) VALUES
(1, '6.000', 1, '25.000', '2019-06-14 00:00:00'),
(2, '8.000', 2, '30.000', '2019-06-12 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hc_d_diario_catalogo`
--

DROP TABLE IF EXISTS `hc_d_diario_catalogo`;
CREATE TABLE `hc_d_diario_catalogo` (
  `id_registro` int(11) NOT NULL,
  `total_b` decimal(10,3) NOT NULL DEFAULT '0.000',
  `total_c` decimal(10,3) NOT NULL DEFAULT '0.000',
  `total_vat` decimal(10,3) NOT NULL DEFAULT '0.000',
  `media_horas` datetime NOT NULL,
  `fecha_entrada` datetime NOT NULL,
  `categoria` char(1) COLLATE utf8_spanish_ci DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hc_d_diario_catalogo`
--

INSERT INTO `hc_d_diario_catalogo` (`id_registro`, `total_b`, `total_c`, `total_vat`, `media_horas`, `fecha_entrada`, `categoria`) VALUES
(1, '45.000', '36.000', '19.000', '0000-00-00 00:00:00', '2019-06-13 00:00:00', 'F');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hc_hist_all`
--

DROP TABLE IF EXISTS `hc_hist_all`;
CREATE TABLE `hc_hist_all` (
  `id_registro` int(11) NOT NULL,
  `fecha_carga` datetime NOT NULL,
  `media_horas` datetime NOT NULL,
  `total_b` decimal(10,3) NOT NULL,
  `total_c` decimal(10,3) NOT NULL,
  `total_vat` decimal(10,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `hc_diario_aux_all`
--
ALTER TABLE `hc_diario_aux_all`
  ADD PRIMARY KEY (`id_registro`,`fecha_carga`);

--
-- Indices de la tabla `hc_d_diario_all`
--
ALTER TABLE `hc_d_diario_all`
  ADD PRIMARY KEY (`id_registro`,`fecha_carga`);

--
-- Indices de la tabla `hc_d_diario_aux`
--
ALTER TABLE `hc_d_diario_aux`
  ADD PRIMARY KEY (`id_registro`),
  ADD KEY `fecha_carga` (`fecha_carga`);

--
-- Indices de la tabla `hc_d_diario_catalogo`
--
ALTER TABLE `hc_d_diario_catalogo`
  ADD PRIMARY KEY (`id_registro`),
  ADD KEY `fecha_entrada` (`fecha_entrada`);

--
-- Indices de la tabla `hc_hist_all`
--
ALTER TABLE `hc_hist_all`
  ADD PRIMARY KEY (`id_registro`),
  ADD KEY `fecha_darga` (`fecha_carga`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `hc_diario_aux_all`
--
ALTER TABLE `hc_diario_aux_all`
  MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `hc_d_diario_all`
--
ALTER TABLE `hc_d_diario_all`
  MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `hc_d_diario_aux`
--
ALTER TABLE `hc_d_diario_aux`
  MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `hc_d_diario_catalogo`
--
ALTER TABLE `hc_d_diario_catalogo`
  MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `hc_hist_all`
--
ALTER TABLE `hc_hist_all`
  MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT;

DELIMITER $$
--
-- Eventos
--
DROP EVENT `LP_HC_D_DIARIO_CATALOGO`$$
CREATE DEFINER=`root`@`localhost` EVENT `LP_HC_D_DIARIO_CATALOGO` ON SCHEDULE EVERY 1 DAY STARTS '2019-06-16 00:00:00' ON COMPLETION PRESERVE ENABLE DO CALL SCN_LD_HC_D_DIARIO_CATALOGO$$

DROP EVENT `LP_HC_D_DIARIO_AUX`$$
CREATE DEFINER=`root`@`localhost` EVENT `LP_HC_D_DIARIO_AUX` ON SCHEDULE EVERY 1 DAY STARTS '2019-06-16 00:00:00' ON COMPLETION PRESERVE ENABLE DO CALL SCN_LD_HC_D_DIARIO_AUX$$

DROP EVENT `LP_HC_HISTORICA`$$
CREATE DEFINER=`root`@`localhost` EVENT `LP_HC_HISTORICA` ON SCHEDULE EVERY 1 YEAR STARTS '2019-06-16 00:00:00' ON COMPLETION PRESERVE ENABLE DO CALL SCN_LD_HC_HIST_ALL$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
