-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2019 a las 14:43:11
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `easy_local`
--
CREATE DATABASE IF NOT EXISTS `easy_local` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `easy_local`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

DROP TABLE IF EXISTS `cuenta`;
CREATE TABLE `cuenta` (
  `id_cuenta` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_restaurante` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `total_b` decimal(10,3) DEFAULT '0.000',
  `total_c` decimal(10,3) DEFAULT '0.000',
  `total_vat` decimal(10,3) DEFAULT '0.000',
  `descripcion` text COLLATE utf8_spanish_ci,
  `fecha_apertura` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fecha_cierre` datetime DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_spanish_ci DEFAULT 'OPEN'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`id_cuenta`, `id_mesa`, `id_restaurante`, `id_usuario`, `total_b`, `total_c`, `total_vat`, `descripcion`, `fecha_apertura`, `fecha_cierre`, `estado`) VALUES
(21, 1112, 1, 1, '11.020', '0.000', '0.000', NULL, '2019-06-14 07:00:00', '2019-06-14 12:00:00', 'PAID'),
(62, 1113, 1, 1, '8.880', '0.000', '0.000', NULL, '2019-06-14 05:00:00', '2019-06-14 09:00:00', 'PAID'),
(63, 1113, 1, 1, '8.350', '0.000', '0.000', NULL, '2019-06-14 14:36:00', '2019-06-14 21:00:00', 'PAID'),
(65, 1113, 1, 1, '11.020', '0.000', '0.000', NULL, '2019-06-14 14:40:08', NULL, 'OPEN'),
(67, 1113, 1, 35, '0.000', '0.000', '0.000', NULL, '2019-06-15 14:34:32', '2019-06-15 00:00:00', 'PAID'),
(69, 1113, 1, 35, '22.450', '0.000', '0.000', NULL, '2019-06-15 23:01:43', '2019-06-16 01:01:45', 'CLOSED');

--
-- Disparadores `cuenta`
--
DROP TRIGGER IF EXISTS `TRG_UPDATE_MESA_USUARIO`;
DELIMITER $$
CREATE TRIGGER `TRG_UPDATE_MESA_USUARIO` BEFORE UPDATE ON `cuenta` FOR EACH ROW BEGIN
	DECLARE CANT_USUARIOS TINYINT(5);
    DECLARE MAX_USUARIOS TINYINT(5);
	IF NEW.ID_MESA <> OLD.ID_MESA THEN
    	UPDATE MESAS M SET M.cant_usuarios = M.cant_usuarios - 1 WHERE M.id_mesa = OLD.ID_MESA;
        UPDATE MESAS M SET M.cant_usuarios = M.cant_usuarios + 1 WHERE M.id_mesa = NEW.ID_MESA;
        
        SELECT M.cant_usuarios, M.max_usuarios INTO CANT_USUARIOS, MAX_USUARIOS FROM MESAS M WHERE M.id_mesa = NEW.ID_MESA;
        
        IF CANT_USUARIOS = MAX_USUARIOS THEN
        	UPDATE MESAS M SET M.ocupada = 1 WHERE M.id_mesa = NEW.ID_MESA;
        ELSE
        	IF CANT_USUARIOS = 0 THEN
            	UPDATE MESAS M SET M.ocupada = 0 WHERE M.id_mesa = NEW.ID_MESA;
            ELSE
            	UPDATE MESAS M SET M.ocupada = 1 WHERE M.id_mesa = NEW.ID_MESA;
            END IF;
        END IF;
        
        SELECT M.cant_usuarios, M.max_usuarios INTO CANT_USUARIOS, MAX_USUARIOS FROM MESAS M WHERE M.id_mesa = OLD.ID_MESA;
        
        IF CANT_USUARIOS = MAX_USUARIOS THEN
        	UPDATE MESAS M SET M.ocupada = 1 WHERE M.id_mesa = OLD.ID_MESA;
        ELSE
        	IF CANT_USUARIOS = 0 THEN
            	UPDATE MESAS M SET M.ocupada = 0 WHERE M.id_mesa = OLD.ID_MESA;
            ELSE
            	UPDATE MESAS M SET M.ocupada = 1 WHERE M.id_mesa = OLD.ID_MESA;
            END IF;
        END IF;
    END IF;
    IF NEW.ESTADO = 'PAID' AND NEW.ID_MESA = OLD.ID_MESA THEN
        UPDATE MESAS M SET M.cant_usuarios = M.cant_usuarios - 1 WHERE M.id_mesa = NEW.ID_MESA;
        
        SELECT M.cant_usuarios, M.max_usuarios INTO CANT_USUARIOS, MAX_USUARIOS FROM MESAS M WHERE M.id_mesa = NEW.ID_MESA;
        
        IF CANT_USUARIOS = MAX_USUARIOS THEN
        	UPDATE MESAS M SET M.ocupada = 1 WHERE M.id_mesa = NEW.ID_MESA;
        ELSE
        	IF CANT_USUARIOS = 0 THEN
            	UPDATE MESAS M SET M.ocupada = 0 WHERE M.id_mesa = NEW.ID_MESA;
            ELSE
            	UPDATE MESAS M SET M.ocupada = 1 WHERE M.id_mesa = NEW.ID_MESA;
            END IF;
        	
        END IF;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `d_tmp_h`
--

DROP TABLE IF EXISTS `d_tmp_h`;
CREATE TABLE `d_tmp_h` (
  `id_tiempo` int(11) NOT NULL DEFAULT '0',
  `tiempo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `entrada_tiempo` time NOT NULL,
  `salida_tiempo` time NOT NULL,
  `cat_d_tmp` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `d_tmp_h`
--

INSERT INTO `d_tmp_h` (`id_tiempo`, `tiempo`, `entrada_tiempo`, `salida_tiempo`, `cat_d_tmp`) VALUES
(1, 'HORARIO_MADRUGADA', '07:00:00', '10:00:00', 'A'),
(2, 'HORARIO_MEDIA_MAÑANA', '10:00:00', '13:00:00', 'B'),
(3, 'HORARIO_COMIDAS', '13:00:00', '16:00:00', 'C'),
(4, 'HORARIO_TARDEO', '16:00:00', '19:00:00', 'D'),
(5, 'HORARIO_CENAS', '19:00:00', '22:00:00', 'E'),
(6, 'HORARIO_POSTCENAS', '22:00:00', '01:00:00', 'F');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesas`
--

DROP TABLE IF EXISTS `mesas`;
CREATE TABLE `mesas` (
  `id_mesa` int(11) NOT NULL,
  `num_mesa` int(11) NOT NULL,
  `cant_usuarios` int(11) DEFAULT '0',
  `max_usuarios` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `ocupada` tinyint(1) NOT NULL DEFAULT '0',
  `id_restaurante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `mesas`
--

INSERT INTO `mesas` (`id_mesa`, `num_mesa`, `cant_usuarios`, `max_usuarios`, `password`, `ocupada`, `id_restaurante`) VALUES
(1112, 1, 0, 4, 'JJJJ', 0, 1),
(1113, 2, 0, 4, 'BBBB', 0, 1),
(1114, 3, 0, 4, 'UUUU', 0, 1),
(1115, 4, 0, 4, 'GGGG', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesas_reservadas`
--

DROP TABLE IF EXISTS `mesas_reservadas`;
CREATE TABLE `mesas_reservadas` (
  `id_m_reserva` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_reserva` int(11) NOT NULL,
  `fecha_actual` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE `pedidos` (
  `id_pedido` int(11) NOT NULL,
  `id_cuenta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_final` decimal(10,2) DEFAULT '0.00',
  `coste_final` decimal(10,3) DEFAULT '0.000',
  `vat_final` decimal(10,3) DEFAULT '0.000',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `id_cuenta`, `id_producto`, `cantidad`, `precio_final`, `coste_final`, `vat_final`, `fecha`) VALUES
(7, 21, 2, 1, '2.96', '0.000', '0.000', '2019-05-22 17:53:14'),
(19, 21, 4, 2, '5.10', '0.000', '0.000', '2019-05-23 07:51:27'),
(20, 21, 2, 1, '2.96', '0.000', '0.000', '2019-05-23 07:51:31'),
(21, 62, 2, 3, '8.88', '0.000', '0.000', '2019-05-29 22:25:41'),
(26, 65, 2, 2, '5.92', '0.000', '0.000', '2019-06-14 14:38:15'),
(27, 65, 4, 2, '5.10', '0.000', '0.000', '2019-06-14 14:38:28'),
(30, 69, 2, 5, '14.80', '0.000', '0.000', '2019-06-15 23:01:40'),
(31, 69, 4, 3, '7.65', '0.000', '0.000', '2019-06-15 23:01:43');

--
-- Disparadores `pedidos`
--
DROP TRIGGER IF EXISTS `TRG_DELETE_CALCULA_TOTAL`;
DELIMITER $$
CREATE TRIGGER `TRG_DELETE_CALCULA_TOTAL` AFTER DELETE ON `pedidos` FOR EACH ROW BEGIN
	DECLARE TOTAL decimal(10,3);
    DECLARE TOTAL_C DECIMAL(10,3);
    DECLARE TOTAL_VAT DECIMAL(10,3);
    
    SET TOTAL = (SELECT SUM(P.precio_final) from pedidos P where P.id_cuenta = OLD.id_cuenta);
    
    SET TOTAL_C = (SELECT SUM(P.coste_final) from pedidos P where P.id_cuenta = OLD.id_cuenta);
    
    SET TOTAL_VAT = (SELECT SUM(P.vat_final) from pedidos P where P.id_cuenta = OLD.id_cuenta);
    
    UPDATE cuenta C SET C.total_b = TOTAL, C.total_c = TOTAL_C, C.total_vat = TOTAL_VAT WHERE C.id_cuenta = OLD.id_cuenta;
    
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_INSERT_CALCULA_PRECIO_PEDIDO`;
DELIMITER $$
CREATE TRIGGER `TRG_INSERT_CALCULA_PRECIO_PEDIDO` BEFORE INSERT ON `pedidos` FOR EACH ROW BEGIN
	DECLARE PRECIO_PRODUCTO decimal(10,2) DEFAULT 0;
    DECLARE COSTE_FINAL DECIMAL(10,3) DEFAULT 0;
    DECLARE VAT_FINAL DECIMAL(10,3) DEFAULT 0;

	SET PRECIO_PRODUCTO = (SELECT P.PRECIO_UNIT*(1+P.VAT) AS PRECIO FROM productos P WHERE P.id_producto = NEW.ID_PRODUCTO);
        
    SET COSTE_FINAL = (SELECT P.coste*(1+P.VAT) AS PRECIO FROM productos P WHERE P.id_producto = NEW.ID_PRODUCTO);
        
    SET VAT_FINAL = (SELECT P.coste*P.VAT AS PRECIO FROM productos P WHERE P.id_producto = NEW.ID_PRODUCTO);

   	SET NEW.PRECIO_FINAL = PRECIO_PRODUCTO * NEW.CANTIDAD;
   
	SET NEW.COSTE_FINAL = COSTE_FINAL * NEW.CANTIDAD;
    
    SET NEW.VAT_FINAL = VAT_FINAL * NEW.CANTIDAD;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_INSERT_CALCULA_TOTAL`;
DELIMITER $$
CREATE TRIGGER `TRG_INSERT_CALCULA_TOTAL` AFTER INSERT ON `pedidos` FOR EACH ROW BEGIN
	DECLARE TOTAL decimal(10,3);
    DECLARE TOTAL_C DECIMAL(10,3);
    DECLARE TOTAL_VAT DECIMAL(10,3);
    
    SET TOTAL = (SELECT SUM(P.precio_final) from pedidos P where P.id_cuenta = NEW.id_cuenta);
    
    SET TOTAL_C = (SELECT SUM(P.coste_final) from pedidos P where P.id_cuenta = NEW.id_cuenta);
    
    SET TOTAL_VAT = (SELECT SUM(P.vat_final) from pedidos P where P.id_cuenta = NEW.id_cuenta);
    
    UPDATE cuenta C SET C.total_b = TOTAL, C.total_c = TOTAL_C, C.total_vat = TOTAL_VAT WHERE C.id_cuenta = NEW.id_cuenta;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_UPDATE_CALCULA_PRECIO_PEDIDO`;
DELIMITER $$
CREATE TRIGGER `TRG_UPDATE_CALCULA_PRECIO_PEDIDO` BEFORE UPDATE ON `pedidos` FOR EACH ROW BEGIN
    DECLARE PRECIO_PRODUCTO decimal(10,2) DEFAULT 0;
    DECLARE COSTE_FINAL DECIMAL(10,3) DEFAULT 0;
    DECLARE VAT_FINAL DECIMAL(10,3) DEFAULT 0;

        SET PRECIO_PRODUCTO = (SELECT P.PRECIO_UNIT*(1+P.VAT) AS PRECIO FROM productos P WHERE P.id_producto = NEW.ID_PRODUCTO);
        
        SET COSTE_FINAL = (SELECT P.coste*(1+P.VAT) AS PRECIO FROM productos P WHERE P.id_producto = NEW.ID_PRODUCTO);
        
        SET VAT_FINAL = (SELECT P.coste*P.VAT AS PRECIO FROM productos P WHERE P.id_producto = NEW.ID_PRODUCTO);

   	SET NEW.PRECIO_FINAL = PRECIO_PRODUCTO * NEW.CANTIDAD;
   
	SET NEW.COSTE_FINAL = COSTE_FINAL * NEW.CANTIDAD;
    
    SET NEW.VAT_FINAL = VAT_FINAL * NEW.CANTIDAD;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_UPDATE_CALCULA_TOTAL`;
DELIMITER $$
CREATE TRIGGER `TRG_UPDATE_CALCULA_TOTAL` AFTER UPDATE ON `pedidos` FOR EACH ROW BEGIN
DECLARE TOTAL decimal(10,3);
    DECLARE TOTAL_C DECIMAL(10,3);
    DECLARE TOTAL_VAT DECIMAL(10,3);
    
    SET TOTAL = (SELECT SUM(P.precio_final) from pedidos P where P.id_cuenta = NEW.id_cuenta);
    
    SET TOTAL_C = (SELECT SUM(P.coste_final) from pedidos P where P.id_cuenta = NEW.id_cuenta);
    
    SET TOTAL_VAT = (SELECT SUM(P.vat_final) from pedidos P where P.id_cuenta = NEW.id_cuenta);
    
    UPDATE cuenta C SET C.total_b = TOTAL, C.total_c = TOTAL_C, C.total_vat = TOTAL_VAT WHERE C.id_cuenta = NEW.id_cuenta;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `id_restaurante` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `precio_unit` decimal(10,2) NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `vat` decimal(10,2) NOT NULL,
  `coste` decimal(10,3) DEFAULT '0.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `id_restaurante`, `nombre`, `imagen`, `precio_unit`, `descripcion`, `vat`, `coste`) VALUES
(1, 1, 'qwe', './images/products/1558469250602.jpg', '45.00', '', '0.21', '0.000'),
(2, 1, 'FLORES', './images/products/1558469250602.jpg', '2.45', '', '0.21', '0.000'),
(4, 1, 'cafe con leche', './images/products/1558469250602.jpg', '2.30', 'nada', '0.11', '0.000'),
(5, 1, 'cafe solo', './images/products/1558469250602.jpg', '2.30', 'otro', '0.11', '0.000'),
(7, 1, 'Priehhh ', './images/products/1558469250602.jpg', '2.30', 'nada', '0.11', '0.000'),
(8, 1, 'coche', './images/products/1558469250602.jpg', '2.30', 'nada', '0.11', '0.000'),
(9, 1, 'casa', './images/products/1558469250602.jpg', '2.30', 'Es un cafe con leche con extra de nata y un poco de canela aromatizada con menta', '0.11', '0.000'),
(10, 1, 'puerta', './images/products/1558353892020.jpg', '2.30', 'nada', '0.11', '0.000');

--
-- Disparadores `productos`
--
DROP TRIGGER IF EXISTS `TRG_UPDATE_PRECIO_PRODUCTO`;
DELIMITER $$
CREATE TRIGGER `TRG_UPDATE_PRECIO_PRODUCTO` AFTER UPDATE ON `productos` FOR EACH ROW BEGIN
	UPDATE pedidos P SET P.precio_final = (P.precio_final-(OLD.PRECIO_UNIT*(1+OLD.VAT)))+(NEW.PRECIO_UNIT*(1+NEW.VAT)), P.coste_final = (P.coste_final - (OLD.COSTE*(1+OLD.VAT)))+(NEW.COSTE*(1+NEW.VAT)) WHERE P.id_producto = NEW.ID_PRODUCTO;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prueba`
--

DROP TABLE IF EXISTS `prueba`;
CREATE TABLE `prueba` (
  `id` int(11) NOT NULL,
  `VALOR` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `prueba`
--

INSERT INTO `prueba` (`id`, `VALOR`) VALUES
(1, '1'),
(2, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

DROP TABLE IF EXISTS `reservas`;
CREATE TABLE `reservas` (
  `id_reserva` int(11) NOT NULL,
  `num_usuarios` int(11) NOT NULL,
  `id_usuario_reserva` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurantes`
--

DROP TABLE IF EXISTS `restaurantes`;
CREATE TABLE `restaurantes` (
  `id_restaurante` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `tipos_comida` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `restaurantes`
--

INSERT INTO `restaurantes` (`id_restaurante`, `id_usuario`, `nombre`, `telefono`, `imagen`, `direccion`, `descripcion`, `tipos_comida`) VALUES
(1, 1, 'PRUEBA RESTAURANTE', NULL, './images/restaurants/1558118773060.jpg', 'calle falsa 123', 'El mejor restaurante de la ciudad', 'Pizzeria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `id_restaurante` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_mesa` int(11) DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'ROLE_USER'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `id_restaurante`, `nombre`, `password`, `telefono`, `id_mesa`, `role`) VALUES
(1, 1, 'Quinella', '81dc9bdb52d04dc20036dbd8313ed055', NULL, 0, 'ROLE_ADMIN'),
(35, 0, 'ben', '81dc9bdb52d04dc20036dbd8313ed055', NULL, 0, 'ROLE_USER');

--
-- Disparadores `usuarios`
--
DROP TRIGGER IF EXISTS `TRG_DELETE_USUARIO`;
DELIMITER $$
CREATE TRIGGER `TRG_DELETE_USUARIO` AFTER DELETE ON `usuarios` FOR EACH ROW BEGIN
	DECLARE TOTAL_USERS TINYINT(3) DEFAULT 0;
    
    SET TOTAL_USERS = (SELECT COUNT(C.id_usuario) AS TOTAL FROM usuarios C WHERE C.id_mesa = OLD.ID_MESA);
    
	UPDATE MESAS M SET M.ocupada = FALSE, M.cant_usuarios = TOTAL_USERS WHERE M.id_mesa = OLD.ID_MESA;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_INSERT_USUARIO`;
DELIMITER $$
CREATE TRIGGER `TRG_INSERT_USUARIO` AFTER INSERT ON `usuarios` FOR EACH ROW BEGIN
	INSERT INTO cuenta VALUES (NULL, 0, 0, NEW.ID_USUARIO, 0, 0, 0, NULL, NULL, NULL, 'OPEN');
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_UPDATE_MESA_USUARIO_INCREMENT`;
DELIMITER $$
CREATE TRIGGER `TRG_UPDATE_MESA_USUARIO_INCREMENT` AFTER UPDATE ON `usuarios` FOR EACH ROW BEGIN
	DECLARE CANT_USUARIOS TINYINT(5);
    DECLARE MAX_USUARIOS TINYINT(5);
    
    IF NEW.ID_MESA = 0 THEN
    	UPDATE MESAS M SET M.cant_usuarios = (SELECT COUNT(*) AS TOTALES FROM USUARIOS C WHERE C.ID_MESA = OLD.ID_MESA) WHERE M.id_mesa = OLD.ID_MESA;
    ELSE
    	UPDATE MESAS M SET M.cant_usuarios = (SELECT COUNT(*) AS TOTALES FROM USUARIOS C WHERE C.ID_MESA = NEW.ID_MESA) WHERE M.id_mesa = NEW.ID_MESA;
    END IF;
    
    SELECT M.cant_usuarios, M.max_usuarios INTO CANT_USUARIOS, MAX_USUARIOS FROM MESAS M WHERE M.id_mesa = NEW.ID_MESA;
        
    IF CANT_USUARIOS = MAX_USUARIOS THEN
    	UPDATE MESAS M SET M.ocupada = 1, M.cant_usuarios = CANT_USUARIOS WHERE M.id_mesa = NEW.ID_MESA;
    ELSE
    	UPDATE MESAS M SET M.ocupada = 0, M.cant_usuarios = CANT_USUARIOS WHERE M.id_mesa = NEW.ID_MESA;
    END IF;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `TRG_UPDATE_USUARIO_PASSWORD`;
DELIMITER $$
CREATE TRIGGER `TRG_UPDATE_USUARIO_PASSWORD` BEFORE UPDATE ON `usuarios` FOR EACH ROW BEGIN
	SET NEW.PASSWORD = OLD.PASSWORD;
END
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`id_cuenta`,`id_mesa`,`id_restaurante`,`id_usuario`),
  ADD KEY `FK_CUENTA_USUARIO` (`id_usuario`);

--
-- Indices de la tabla `mesas`
--
ALTER TABLE `mesas`
  ADD PRIMARY KEY (`id_mesa`,`id_restaurante`) USING BTREE,
  ADD KEY `FK_MESA_RESTAURANTE` (`id_restaurante`);

--
-- Indices de la tabla `mesas_reservadas`
--
ALTER TABLE `mesas_reservadas`
  ADD PRIMARY KEY (`id_m_reserva`,`id_mesa`,`id_reserva`),
  ADD KEY `FK_M_R_MESAS` (`id_mesa`),
  ADD KEY `FK_M_R_RESERVA` (`id_reserva`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`,`id_cuenta`,`id_producto`,`fecha`),
  ADD KEY `FK_PEDIDO_CUENTA` (`id_cuenta`),
  ADD KEY `FK_PEDIDO_PRODUCTO` (`id_producto`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`,`id_restaurante`),
  ADD KEY `FK_PRODUCTO_RESTAURANTE` (`id_restaurante`);

--
-- Indices de la tabla `prueba`
--
ALTER TABLE `prueba`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD PRIMARY KEY (`id_reserva`),
  ADD KEY `FK_RESERVA_USUARIO` (`id_usuario_reserva`);

--
-- Indices de la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  ADD PRIMARY KEY (`id_restaurante`,`id_usuario`),
  ADD KEY `FK_RESTAURANTE_USUARIO` (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`) USING BTREE,
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `id_cuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT de la tabla `mesas`
--
ALTER TABLE `mesas`
  MODIFY `id_mesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1116;

--
-- AUTO_INCREMENT de la tabla `mesas_reservadas`
--
ALTER TABLE `mesas_reservadas`
  MODIFY `id_m_reserva` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `prueba`
--
ALTER TABLE `prueba`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reservas`
--
ALTER TABLE `reservas`
  MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  MODIFY `id_restaurante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `FK_CUENTA_USUARIO` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mesas`
--
ALTER TABLE `mesas`
  ADD CONSTRAINT `FK_MESA_RESTAURANTE` FOREIGN KEY (`id_restaurante`) REFERENCES `restaurantes` (`id_restaurante`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mesas_reservadas`
--
ALTER TABLE `mesas_reservadas`
  ADD CONSTRAINT `FK_M_R_MESAS` FOREIGN KEY (`id_mesa`) REFERENCES `mesas` (`id_mesa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_M_R_RESERVA` FOREIGN KEY (`id_reserva`) REFERENCES `reservas` (`id_reserva`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `FK_PEDIDO_CUENTA` FOREIGN KEY (`id_cuenta`) REFERENCES `cuenta` (`id_cuenta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PEDIDO_PRODUCTO` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `FK_PRODUCTO_RESTAURANTE` FOREIGN KEY (`id_restaurante`) REFERENCES `restaurantes` (`id_restaurante`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD CONSTRAINT `FK_RESERVA_USUARIO` FOREIGN KEY (`id_usuario_reserva`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  ADD CONSTRAINT `FK_RESTAURANTE_USUARIO` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
