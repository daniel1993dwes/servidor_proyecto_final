const conexion = require("../config/bdconfig");
const md5 = require("md5");
const jwt = require("jsonwebtoken");
const constant = require("../constants/constants");
const secreto = constant.secreto;

module.exports = class Token {
  static generarToken(id, role, rest) {
    return jwt.sign({ id: id, role: role, rest: rest }, secreto, { expiresIn: "3 hours" });
  };

  static validarToken(token) {
    let tokenSinBearer = this.eliminaBearer(token);
    try {
      let resultado = jwt.verify(tokenSinBearer, secreto);
      return resultado;
    } catch (e) {}
  }

  static eliminaBearer(token) {
    let tokenWithoutBearer = token.split(' ');
    return tokenWithoutBearer[1];
  }
};
