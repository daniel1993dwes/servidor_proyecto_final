let mysql = require("mysql");

let conexion = mysql.createConnection({
  host: "localhost",
  user: "root",
  port: 3306,
  password: "",
  database: "easy_local"
});

conexion.connect(error => {
  if (error) console.log("Error al conectar con la BD:", error);
  else console.log("Bienvenido al sistema administrador del SNS");
});

module.exports = conexion;