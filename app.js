const express = require("express");
const bodyParser = require("body-parser");
const passport = require("passport");
const { Strategy, ExtractJwt } = require("passport-jwt");
const usuarios = require("./routes/users");
const auth = require('./routes/auth');
const cuentas = require('./routes/cuentas');
const mesas = require('./routes/mesas');
const pedidos = require('./routes/pedidos');
const restaurantes = require('./routes/restaurantes');
const productos = require('./routes/productos');
const pruebas = require('./routes/pruebas');
const estudios = require('./routes/estudios');
const sharp = require('sharp');
const cors = require('cors');

const constant = require('./constants/constants');

const secreto = constant.secreto;

let app = express();
app.use(cors());
app.use(passport.initialize());
app.use(bodyParser.json());
app.use('/images/', express.static('images'));

passport.use(
  new Strategy(
    {
      secretOrKey: secreto,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },
    (payload, done) => {
      if (payload.id) {
        return done(null, { id: payload.id, role: payload.role, rest: payload.rest });
      } else {
        return done(new Error("Usuario incorrecto"), null);
      }
    }
  )
);

app.use('/auth', auth);
app.use('/users', passport.authenticate('jwt', { session: false }), usuarios);
app.use('/cuentas', passport.authenticate('jwt', { session: false }), cuentas);
app.use('/mesas', passport.authenticate('jwt', { session: false }), mesas);
app.use('/pedidos', passport.authenticate('jwt', { session: false }), pedidos);
app.use('/restaurantes', passport.authenticate('jwt', { session: false }), restaurantes);
app.use('/productos', passport.authenticate('jwt', { session: false }), productos);
app.use('/estudios', passport.authenticate('jwt', { session: false }), estudios);
app.use('/pruebas', pruebas);

app.listen(3000);
